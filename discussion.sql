INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);    

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- JUSTIN BIEBER
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 192, "Dancehall-poptropical housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

-- ARIANA GRANDE
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 9);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 196, "Pop, R&B", 10);
-- Bruno Mars:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 192, "Pop", 12);


-- [SECTION]

SELECT FROM songs WHERE id != 3;

-- Greter than or equal
SELECT * FROM songs Where id > 3;

SELECT * FROM songs Where id >= 3;

-- LESS THAN or EQUAL
SELECT * FROM song WHERE id < 11;

SELECT * FROM song WHERE id <= 11;

-- In operator
SELECT * FROM songs where id IN (1,3,11)

SELECT * FROM songs where genre IN ("pop", "kpop")

-- partial matches.

SELECT * FROM songs WHERE song_name LIKE "%a";

SELECT * FROM songs WHERE song_name LIKE "%not";

SELECT * FROM songs WHERE song_name LIKE "a%";

-- basta may letter a
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- find MASAYA

SELECT * FROM songs WHERE song_name LIKE "__s_y_";

SELECT * FROM songs WHERE song_name LIKE "%%s_y%";

-- sorting records

SELECT * FROM songs ORDER BY song_name ASC;

SELECT * FROM songs ORDER BY song_name DESC;

-- getting distinct record
SELECT DISTINCT genre FROM songs;

-- [INNNER JOIN]
SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id;


SELECT albums.album_title, songs.song_name FROM albums JOIN songs ON albums.id = songs.album_id

-- LEFT JOIN
SELECT * albums.album_title, songs.song_name FROM albums LEFT JOIN songs ON albums.id = songs.album_id;

-- JOINUING Multiple tables
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id
    JOIN songs ON albums.id = songs.albums.id;


