-- find all artists that have letter d in its name.
SELECT * FROM artists WHERE name LIKE '%d%';

-- find all songs that have a length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- join albums and songs tables
SELECT albums.album_title, songs.song_name, songs.length FROM albums INNER JOIN songs ON albums.id = songs.album_id;

-- join artists and albums tables
SELECT artists.name, albums.album_title 
FROM artists 
INNER JOIN albums ON artists.id = albums.artist_id 
WHERE albums.album_title LIKE '%a%';

-- sort the albums in Z-A order(show only the first 4 records)
SELECT artists.name, albums.album_title 
FROM artists 
INNER JOIN albums ON artists.id = albums.artist_id 
WHERE albums.album_title LIKE '%a%';